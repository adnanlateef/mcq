package com.deamonixi;

import javax.servlet.Filter;

import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;

import com.deamonixi.config.AppConfiguration;
import com.deamonixi.config.DBConfiguration;
import com.deamonixi.config.RootConfig;
import com.deamonixi.filter.AppServletFilter;

public class ServletInitializer {// extends
	// AbstractAnnotationConfigDispatcherServletInitializer {

	// @Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { DBConfiguration.class, RootConfig.class };
	}

	// @Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { AppConfiguration.class };
	}

	// @Override
	protected Filter[] getServletFilters() {
		Filter applicationFilter = new AppServletFilter();

		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);
		return new Filter[] { applicationFilter, characterEncodingFilter,
				new HiddenHttpMethodFilter() };
	}

	// @Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
