package com.deamonixi.config;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.deamonixi.dao" })
public class DBConfiguration {
	static Logger log = LoggerFactory.getLogger(DBConfiguration.class);

	@Bean
	public static SessionFactory getSessionFactory() {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		try {
			localSessionFactoryBean.setDataSource(dataSource());
			localSessionFactoryBean
					.setPackagesToScan(new String[] { "com.deamonixi" });
			localSessionFactoryBean
					.setHibernateProperties(hibernateProperties());
			// Added the below line
			localSessionFactoryBean.afterPropertiesSet();
			log.debug("SESSION FACTORY BEAN CREATED"
					+ localSessionFactoryBean.getObject());

			HibernateTransactionManager txManager = new HibernateTransactionManager();
			txManager.setSessionFactory(localSessionFactoryBean.getObject());
		} catch (IOException e) {
			log.debug("Unable to create Session Factory");
		}
		return localSessionFactoryBean.getObject();
	}

	@Bean
	private static DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource
				.setUrl("jdbc:mysql://localhost:3306/mcqs?createDatabaseIfNotExist=true");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}

	private static Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect",
				"org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.format_sql", "true");
		// properties.put("hibernate.hbm2ddl.auto", "create");
		// properties.put("hibernate.hbm2ddl.import_files", "import.sql");
		// properties.put("hibernate.temp.use_jdbc_metadata_defaults", "false");
		// properties.put("hibernate.jdbc.lob.non_contextual_creation", "true");
		return properties;
	}

}
