package com.deamonixi.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.deamonixi.dao.AnswerDao;
import com.deamonixi.dao.QuestionCategoryDao;
import com.deamonixi.dao.QuestionDao;

//@ComponentScan(basePackages = { "com.deamonixi.service" })
@Configuration
public class RootConfig {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Bean
	public QuestionCategoryDao getQuestionCategoryDao() {
		return new QuestionCategoryDao();
	}

	@Bean
	public QuestionDao getQuestionDao() {
		return new QuestionDao();
	}

	@Bean
	public AnswerDao getAnswerDao() {
		return new AnswerDao();
	}

}
