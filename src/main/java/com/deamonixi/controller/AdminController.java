package com.deamonixi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.deamonixi.service.QuestionService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	QuestionService questionService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView viewAdminPage() {
		return new ModelAndView("Admin");
	}

	@RequestMapping(value = "/addCategories", method = RequestMethod.POST)
	public ModelAndView addDefaultCategories() {
		questionService.addDefaultCategories();
		return viewAdminPage();
	}
}
