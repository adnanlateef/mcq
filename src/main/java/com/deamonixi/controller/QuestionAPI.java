package com.deamonixi.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.deamonixi.model.Question;
import com.deamonixi.model.QuestionCategory;
import com.deamonixi.service.APIServiceImpl;
import com.deamonixi.service.QuestionService;

@RestController
@RequestMapping(value = "/api")
public class QuestionAPI {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	APIServiceImpl apiService;

	@Autowired
	QuestionService questionService;

	// list questions with no images
	// out image only
	// gson
	// image quality
	// compress json
	@RequestMapping(value = "/lastmodified/{categoryId}")
	public QuestionCategory categoryLastModified(HttpServletRequest request,
			HttpServletResponse response, @PathVariable String categoryId)
			throws IOException {
		return questionService.getQuestionCategory(categoryId);
	}

	@RequestMapping(value = "/questions/{categoryId}/{dateModified}", method = RequestMethod.GET)
	public List<Question> questionsAfterModifiedDateForCategory(
			HttpServletRequest request, HttpServletResponse response,
			@PathVariable long categoryId, @PathVariable String dateModified)
			throws IOException, ParseException {
		return apiService.getQuestionsForCategoryAfterModifiedTime(categoryId,
				dateModified);
	}

	@RequestMapping(value = "/image/{questionId}/{answerid}", method = RequestMethod.GET)
	public void getAnswerImage(HttpServletRequest req,
			HttpServletResponse response, @PathVariable Long questionId,
			@PathVariable Long answerId) throws IOException {
		byte[] image = apiService.getAnswer(questionId, answerId).getImage();
		response.getOutputStream().write(image);
		response.getOutputStream().flush();
		response.setContentType("image/jpeg");
		response.setContentLength(image.length);
		response.addHeader("Cache-Control", "max-age=3600");
	}

	@RequestMapping(value = "/image/{questionId}", method = RequestMethod.GET)
	public void getQuestionImage(HttpServletRequest req,
			HttpServletResponse response, @PathVariable Long questionId)
			throws IOException {
		byte[] image = apiService.getQuestion(questionId).getImage();
		response.getOutputStream().write(image);
		response.getOutputStream().flush();
		response.setContentType("image/jpeg");
		response.setContentLength(image.length);
		response.addHeader("Cache-Control", "max-age=3600");
	}

	@RequestMapping(value = "/answers/{categoryId}")
	public void answersForCategory(HttpServletRequest request,
			HttpServletResponse response, @PathVariable String categoryId)
			throws IOException {
		response.getOutputStream().write(0);
	}
}
