package com.deamonixi.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.deamonixi.model.Question;
import com.deamonixi.service.QuestionService;

@Controller
public class QuestionExploreController {

	@Autowired
	private QuestionService questionService;

	@ModelAttribute(value = "ListQuestions")
	public Collection<Question> getQuestions() {
		return questionService.getAllQuestions();
	}

	@RequestMapping(value = "/list/questions", method = RequestMethod.GET)
	public ModelAndView getAllQuestions(
			@ModelAttribute(value = "ListQuestions") Collection<Question> questions) {
		return new ModelAndView("ListQuestion");
	}

	@RequestMapping(value = "/list/questions/{categoryId}", method = RequestMethod.GET)
	public ModelAndView getAllQuestionsForCategory(
			@ModelAttribute(value = "ListQuestions") Collection<Question> questions) {
		return new ModelAndView("ListQuestion");
	}

}
