package com.deamonixi.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.deamonixi.model.Answer;
import com.deamonixi.model.Question;
import com.deamonixi.service.QuestionService;
import com.deamonixi.util.Utility;

@Controller
public class QuestionManipulationController {
	Logger log = LoggerFactory.getLogger(this.getClass());

	// @Autowired
	// private ModerateService moderateService;

	@Autowired
	private QuestionService questionService;

	@ModelAttribute(value = "Question")
	public Question getQuestion(String id) {
		if (id == null) {
			log.debug("Returning new Question.");
			return new Question();
		}
		return questionService.getQuestion(id);
	}

	@RequestMapping(value = "/add/question", method = RequestMethod.GET)
	public ModelAndView addQuestion(
			@ModelAttribute(value = "Question") Question question) {
		ModelAndView mv = new ModelAndView("AddQuestion");
		log.debug(question.toString());
		List<Answer> answers = question.getAnswers();
		if (answers == null || answers.size() < 1) {
			question.initializeAnswers();
		}
		mv.addObject("Question", question);
		mv.addObject("categories",
				Utility.getMapFromCategoryList(questionService
						.getQuestionCategories()));
		return mv;
	}

	@RequestMapping(value = "/add/question", method = RequestMethod.POST)
	public ModelAndView postQuestion(
			@ModelAttribute(value = "Question") Question question,
			BindingResult result, Model model) throws IOException {

		if (result.hasErrors()) {
			if (result.hasFieldErrors("questionId")) {
				result.reject("questionId");
				log.debug("ADDITION...");
			}
		}
		extractImagesIfAny(question);
		question.setDateModified(new Date());
		List<Answer> answers = question.getAnswers();
		for (Answer answer : answers) {
			log.debug("Answer:" + answer);
			answer.setQuestion(question);
		}
		questionService.saveQuestion(question);
		log.debug(String.format("Updating category:%s", question.getCategory()));
		questionService.updateCategoryTimestamp(question.getCategory());
		return new ModelAndView("redirect:/add/question");
	}

	@RequestMapping(value = "/show/question", method = RequestMethod.POST)
	public ModelAndView postAddShowQuestion(
			@ModelAttribute(value = "Question") Question question,
			BindingResult result, Model model) throws IOException {
		if (result.hasErrors()) {
			if (result.hasFieldErrors("questionId")) {
				log.debug("ADDITION...");
			}
		}
		extractImagesIfAny(question);
		question.setDateModified(new Date());
		List<Answer> answers = question.getAnswers();
		for (Answer answer : answers) {
			log.debug("Answer:" + answer);
			answer.setQuestion(question);
		}
		long questionId = questionService.saveQuestion(question);
		log.debug(String.format("Updating category:%s", question.getCategory()));
		questionService.updateCategoryTimestamp(question.getCategory());
		return new ModelAndView(String.format("redirect:/show/question/%d",
				questionId));
	}

	private void extractImagesIfAny(Question question) throws IOException {
		if (question.getFile() != null
				&& !question.getFile().getOriginalFilename().isEmpty()) {
			question.setImage(Utility.getImageByteArray(question.getFile()));
		}
		List<Answer> answers = question.getAnswers();
		for (Answer answer : answers) {
			if (answer.getFile() != null
					&& !answer.getFile().getOriginalFilename().isEmpty()) {
				answer.setImage(Utility.getImageByteArray(answer.getFile()));
			}
		}
	}

	@RequestMapping(value = "/show/question/{id}", method = RequestMethod.GET)
	public ModelAndView showQuestion(@PathVariable String id) {
		ModelAndView mv = new ModelAndView("ShowQuestion");
		mv.addObject("Question", questionService.getQuestion(id));
		return mv;
	}

	@RequestMapping(value = "/delete/question/{id}", method = RequestMethod.GET)
	public ModelAndView deleteQuestion(@PathVariable String id) {
		Question question = questionService.getQuestion(id);
		questionService.updateCategoryTimestamp(question.getCategory());
		questionService.deleteQuestion(question);
		return new ModelAndView("redirect:/list/questions");
	}

	@RequestMapping(value = "/edit/question/{id}", method = RequestMethod.GET)
	public ModelAndView editQuestion(@PathVariable String id) {
		// question.setDateModified(new Date());
		// questionService.updateQuestion(question);
		// questionService.updateCategoryTimestamp(question.getCategory());
		return addQuestion(questionService.getQuestion(id));
	}

	@RequestMapping(value = "/thumbnail/{type}/{id}", method = RequestMethod.GET)
	public void getThumbnail(@PathVariable String type,
			@PathVariable String id, HttpServletRequest req,
			HttpServletResponse response) {
		byte[] image = new byte[0];
		if ("answer".equals(type)) {
			Question question = questionService.getQuestion(id);
			if (question == null || question.getImage() == null) {
				return;
			}
			Answer answer = question.getAnswers().parallelStream()
					.filter(a -> a.isCorrect()).findAny().get();
			image = answer.getImage();
		} else {
			Question question = questionService.getQuestion(id);
			if (question == null || question.getImage() == null) {
				return;
			}
			image = question.getImage();
		}
		// TODO: buffer
		try {
			response.getOutputStream().write(image);
			response.getOutputStream().flush();
			response.setContentType("image/jpeg");
			response.setContentLength(image.length);
			response.addHeader("Cache-Control", "max-age=3600");

		} catch (IOException e) {
			log.error("Problem when getting the image: {}", e.getMessage());
		}

	}

}
