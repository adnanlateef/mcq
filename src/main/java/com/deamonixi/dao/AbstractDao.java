package com.deamonixi.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractDao {
	Logger log = LoggerFactory.getLogger(this.getClass());

	SessionFactory sf = HibernateUtil.getInstance().getSessionFactory();

	public Session openSession() {
		return sf.openSession();
	}

	public <T> List<T> getCriteriaResult(Criteria c) {
		try {
			return c.list();
		} catch (Exception e) {
			log.debug("Probelm fetching results using criteria.", e);
		} finally {
			// sf.close();
		}
		return Collections.emptyList();
	}

	public Long save(Object obj) {
		Session session = getTransactionedSession();

		Long id = (Long) session.save(obj);

		session.getTransaction().commit();
		session.close();
		return id;
	}

	public void saveOrUpdate(Object obj) {
		Session session = getTransactionedSession();
		session.saveOrUpdate(obj);
		session.getTransaction().commit();
		session.close();
	}

	protected Session getTransactionedSession() {
		Session session = sf.openSession();
		session.beginTransaction();
		return session;
	}

	public <T> List<T> get(Class<T> clazz) {
		return get(clazz, null, 0);
	}

	public <T> List<T> get(Class<T> clazz, int limit) {
		return get(clazz, null, limit);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> get(Class<T> clazz, String orderBy, int limit) {
		log.debug("getting for {}", clazz.toString());
		Session session = sf.openSession();
		try {
			Criteria cri = session.createCriteria(clazz);
			if (orderBy != null && !orderBy.isEmpty()) {
				cri.addOrder(Order.asc(orderBy));
			}
			if (limit > 0) {
				cri.setMaxResults(limit);
			}
			return cri.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return new ArrayList<T>();
	}

	public void update(Object obj) {
		Session session = getTransactionedSession();
		session.update(obj);
		session.getTransaction().commit();
		session.close();

	}

	public <T> T get(Class<T> clazz, long id) {
		Session session = sf.openSession();
		T t = (T) session.get(clazz, id);
		session.close();
		return t;
	}

	@SuppressWarnings("rawtypes")
	public List get(String hqlTableName, Long id) {
		Session session = getTransactionedSession();
		Query query = session.createQuery(" from " + hqlTableName
				+ " as p where p.id=" + id);

		List l = query.list();

		session.getTransaction().commit();
		session.close();
		return l;
	}

	public void delete(Object obj) {
		Session session = getTransactionedSession();
		session.delete(obj);
		session.getTransaction().commit();
		session.close();

	}

	public void executeInTransaction(HibernateTransactionConsumer callable) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sf.openSession();
			tx = session.beginTransaction();
			callable.accept(session);
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public <T> T executeInTransaction(HibernateTransactionFunction<T> callable) {
		T result = null;
		Session session = null;
		Transaction tx = null;
		try {
			session = sf.openSession();
			tx = session.beginTransaction();
			result = callable.apply(session);
			tx.commit();
		} catch (RuntimeException re) {
			if (tx != null) {
				tx.rollback();
			}
			throw re;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}

}

@FunctionalInterface
interface HibernateTransactionConsumer extends Consumer<Session> {

}

@FunctionalInterface
interface HibernateTransactionFunction<T> extends Function<Session, T> {

}
