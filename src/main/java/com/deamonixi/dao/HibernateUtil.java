package com.deamonixi.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.deamonixi.config.DBConfiguration;

@Component
public class HibernateUtil {
	Logger log = LoggerFactory.getLogger(this.getClass());

	private static HibernateUtil hibernateUtil = null;

	SessionFactory sessionFactory = DBConfiguration.getSessionFactory();

	private HibernateUtil() {
	}

	public static HibernateUtil getInstance() {
		if (hibernateUtil == null) {
			hibernateUtil = new HibernateUtil();
		}
		return hibernateUtil;
	}

	public Session getSession() {
		Session session = getSessionFactory().getCurrentSession();
		if (session == null)
			session = getSessionFactory().openSession();
		return session;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void shutdown() {
		getSessionFactory().close();
	}

}