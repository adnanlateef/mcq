package com.deamonixi.dao;

import java.util.Date;
import java.util.List;

import com.deamonixi.model.QuestionCategory;

public class QuestionCategoryDao extends AbstractDao {

	public QuestionCategoryDao() {
	}

	public void addDefaultCategories() {
		QuestionCategory category = null;
		Date mDate = new Date();
		for (int i = 0; i < 4; i++) {
			category = new QuestionCategory("Category-" + i, String.format(
					"Categor %d Text", i));
			category.setDateModified(mDate);
			super.saveOrUpdate(category);
		}
	}

	public List<QuestionCategory> getQuestionCategories() {
		return super.get(QuestionCategory.class);
	}
}
