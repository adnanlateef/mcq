package com.deamonixi.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.deamonixi.model.Answer;
import com.deamonixi.model.Question;
import com.deamonixi.model.QuestionCategory;

public class QuestionDao extends AbstractDao {

	public List<Question> getQuestionModifiedAfter(QuestionCategory category,
			Date dateModified) {
		boolean useQuery = true;
		if (useQuery) {
			return getQuestions(category, dateModified);
		} else {
			Session session = super.openSession();
			Criteria criteria = session.createCriteria(Question.class, "q");
			criteria.add(Restrictions.eq("q.category", category));
			criteria.add(Restrictions.gt("q.dateModified", dateModified));

			List<Question> criteriaResult = criteria.list();
			session.clear();
			session.close();
			for (Question question : criteriaResult) {
				log.debug(question.toString());
			}
			log.debug("Total questions:" + criteriaResult.size());
			return criteriaResult;
		}
	}

	private List<Question> getQuestions(QuestionCategory category,
			Date dateLastModified) {
		String query = "from Question q where q.dateModified > :dateLastModified and q.category = :category";
		Session session = super.openSession();
		Query q = session.createQuery(query);
		// q.setParameter("category", category);
		q.setParameter("dateLastModified", dateLastModified);
		q.setParameter("category", category);

		return q.list();
	}

	@SuppressWarnings("unchecked")
	public List<Question> getQuestions(QuestionCategory qc) {
		return super
				.get("from Question q, QuestionCategory c where q.categoryId=c.id AND q.categoryId = :qc",
						qc.getCategoryId());
	}

	public Question getQuestion(Answer ans) {
		return (Question) super
				.get("from Question q, Answer a where q.id=a.questionId AND a.answerId=:ans",
						ans.getAnswerId()).get(0);
	}

	public Answer getAnswerWithImage(Question question, Answer answer) {
		return (Answer) super
				.get("from Question q, Answer a where q.id=a.questionId AND a.answerId=",
						answer.getAnswerId()).get(0);
	}
}
