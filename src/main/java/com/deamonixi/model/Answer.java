package com.deamonixi.model;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Entity
@Table(name = "answer")
public class Answer {

	@Id
	@GeneratedValue
	private long answerId;

	@ManyToOne
	private Question question;

	@NotNull
	private String text;
	@Lob
	private byte[] image;

	private boolean correct;
	@Transient
	private CommonsMultipartFile file;
	@Transient
	private String imageString;

	public Answer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Answer(String text, byte[] image) {
		super();
		this.text = text;
		this.image = image;
	}

	public Long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Long answerId) {
		this.answerId = answerId;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;

	}

	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}

	public String getImageString() {
		return new String(image == null ? new byte[] {} : image);
	}

	public void setImageString(String imageString) {
		this.imageString = imageString;
	}

	public CommonsMultipartFile getFile() {
		return file;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Answer [answerId=").append(answerId)
				.append(", question=").append(question.getQuestionId())
				.append(", text=").append(text).append(", image=")
				.append(Arrays.toString(image)).append(", file=").append(file)
				.append("]");
		return builder.toString();
	}

}
