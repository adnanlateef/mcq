package com.deamonixi.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.deamonixi.model.json.QuestionSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "question")
@JsonSerialize(using = QuestionSerializer.class)
public class Question {

	@Id
	@GeneratedValue
	private long questionId;

	@NotNull
	private String title;
	@NotNull
	private String text;
	@Lob
	private byte[] image;
	private Date dateModified;

	@ManyToOne
	private QuestionCategory category;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "question", orphanRemoval = true, fetch = FetchType.EAGER)
	private List<Answer> answers = new ArrayList<Answer>();

	// Transient stuff
	@Transient
	private CommonsMultipartFile file;
	@Transient
	private String imageString;

	public Question() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Question(String title, String text, byte[] image,
			List<Answer> answers, Answer correctAnswer) {
		super();
		this.title = title;
		this.text = text;
		this.image = image;
		// this.answers = answers;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public void addAnswer(Answer answer) {
		if (this.answers == null) {
			this.answers = new ArrayList<Answer>();
		}
		this.answers.add(answer);
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> otherOptions) {
		this.answers = otherOptions;
	}

	public QuestionCategory getCategory() {
		return category;
	}

	public void setCategory(QuestionCategory category) {
		this.category = category;
	}

	public CommonsMultipartFile getFile() {
		return file;
	}

	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}

	public String getImageString() {
		return new String(image == null ? new byte[] {} : image);
	}

	public void setImageString(String imageString) {
		this.imageString = imageString;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Question [questionId=").append(questionId)
				.append(", title=").append(title).append(", text=")
				.append(text).append(", category=").append(category)
				.append(", image=").append((image != null) ? image.length : -1)
				.append(", answers=").append(answers.size()).append(", file=")
				.append(file).append("]");
		return builder.toString();
	}

	public static String collectionName() {
		return "question";
	}

	public void initializeAnswers() {
		answers = new ArrayList<Answer>();
		for (int i = 0; i < 4; i++) {
			answers.add(new Answer());
		}
	}

}
