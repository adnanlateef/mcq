package com.deamonixi.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "question_category")
public class QuestionCategory {

	public QuestionCategory() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue
	private Long categoryId;

	@NotNull
	private String name;

	private String text;
	@NotNull
	private Date dateModified;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "category", orphanRemoval = true)
	private final List<Question> questions = new ArrayList<Question>();

	public QuestionCategory(String name, String text) {
		super();
		this.name = name;
		this.text = text;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	// public List<Question> getQuestions() {
	// return questions;
	// }
	//
	// public void setQuestions(List<Question> questions) {
	// this.questions = questions;
	// }

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("QuestionCategory [categoryId=").append(categoryId)
				.append(", name=").append(name).append(", text=").append(text)
				.append("]");
		return builder.toString();
	}

	public static String collectionName() {
		// TODO Auto-generated method stub
		return "questionCategory";
	}
}
