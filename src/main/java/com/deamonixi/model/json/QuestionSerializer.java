package com.deamonixi.model.json;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.datetime.DateFormatter;

import com.deamonixi.model.Answer;
import com.deamonixi.model.Question;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class QuestionSerializer extends JsonSerializer<Question> {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void serialize(Question question, JsonGenerator jsonGen,
			SerializerProvider serProvider) throws IOException,
			JsonProcessingException {

		try {
			jsonGen.writeStartObject();
			jsonGen.writeNumberField("questionId", question.getQuestionId());
			jsonGen.writeStringField("title", question.getTitle());
			jsonGen.writeStringField("text", question.getText());
			DateFormatter df = new DateFormatter("yyyyMMddHHmmss");
			jsonGen.writeStringField("dateModified",
					df.print(question.getDateModified(), Locale.getDefault()));
			jsonGen.writeBooleanField("hasImage", question.getImage() != null
					&& question.getImage().length > 0);
			jsonGen.writeArrayFieldStart("answers");
			List<Answer> answers = question.getAnswers();
			for (Answer answer : answers) {
				jsonGen.writeStartObject();
				jsonGen.writeNumberField("answerId", answer.getAnswerId());
				jsonGen.writeStringField("text",
						Objects.toString(answer.getText(), ""));
				jsonGen.writeBooleanField("correct", answer.isCorrect());
				jsonGen.writeBooleanField("hasImage", answer.getImage() != null
						&& answer.getImage().length > 0);
				jsonGen.writeEndObject();
			}
			jsonGen.writeEndArray();
			jsonGen.writeEndObject();
		} catch (Exception e) {
			log.error("Exception creating Json.", e);
		}
	}
}
