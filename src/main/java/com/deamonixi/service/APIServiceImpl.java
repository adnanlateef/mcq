package com.deamonixi.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.stereotype.Service;

import com.deamonixi.dao.QuestionCategoryDao;
import com.deamonixi.dao.QuestionDao;
import com.deamonixi.model.Question;
import com.deamonixi.model.QuestionCategory;

@Service
public class APIServiceImpl implements APIService {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	QuestionDao questionDao;
	@Autowired
	QuestionCategoryDao questionCategoryDao;

	public List<Question> getQuestionsForCategoryAfterModifiedTime(
			long categoryId, String lastModified) throws ParseException {

		QuestionCategory category = questionCategoryDao.get(
				QuestionCategory.class, categoryId);
		log.debug("Getting questions for {}", category);
		Date dateModified = new Date(0);
		if (lastModified != null && !"0".equals(lastModified)) {
			DateFormatter df = new DateFormatter("yyyyMMddHHmmss");
			dateModified = df.parse(lastModified, Locale.getDefault());
		}
		return questionDao.getQuestionModifiedAfter(category, dateModified);
	}

	public Question getQuestion(Long questionId, String lastSynced) {
		return questionDao.get(Question.class, questionId);
	}

	public Question getAnswer(Long questionId, Long answerId) {
		// TODO Auto-generated method stub
		return null;
	}

	public Question getQuestion(Long questionId) {
		// TODO Auto-generated method stub
		return null;
	}

}
