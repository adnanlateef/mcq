package com.deamonixi.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deamonixi.dao.AnswerDao;
import com.deamonixi.dao.QuestionCategoryDao;
import com.deamonixi.dao.QuestionDao;
import com.deamonixi.model.Answer;
import com.deamonixi.model.Question;
import com.deamonixi.model.QuestionCategory;

@Service
public class QuestionService {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	AnswerDao answerRepo;
	@Autowired
	QuestionDao questionRepo;
	@Autowired
	QuestionCategoryDao questionCategoryRepo;

	public List<QuestionCategory> getQuestionCategories() {
		return questionCategoryRepo.get(QuestionCategory.class);
	}

	public void addDefaultCategories() {
		questionCategoryRepo.addDefaultCategories();
	}

	public Question getQuestion(String id) {
		return questionRepo.get(Question.class, Long.parseLong(id));
	}

	//
	public Collection<Question> getAllQuestions() {
		return questionRepo.get(Question.class);
	}

	//
	public long saveQuestion(Question question) {
		return questionRepo.save(question);
	}

	//
	public void updateQuestion(Question question) {
	}

	//
	public void updateCategoryTimestamp(QuestionCategory category) {
		category = questionCategoryRepo.get(QuestionCategory.class,
				category.getCategoryId());
		category.setDateModified(new Date());
		questionCategoryRepo.update(category);
	}

	//
	public Answer getAnswer(String answerId) {
		return null;
	}

	//
	public void deleteById(String id) {
	}

	//
	public void deleteQuestion(Question question) {
	}

	//
	public QuestionCategory getQuestionCategory(String categoryId) {
		return null;
	}

}
