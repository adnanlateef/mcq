package com.deamonixi.util;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.deamonixi.model.QuestionCategory;

public class Utility {
	static Logger log = LoggerFactory.getLogger(Utility.class);
	static int DEFAULT_HEIGHT = 100;
	static int DEFAULT_WIDTH = 100;

	public final static String SESSION_USER = "USER";

	public static String buildFileStr = "<project name=\"MyProject\" default=\"dist\"> "
			+ "<target name=\"dist\">"
			+ "<symlink failonerror=\"false\" action=\"delete\" link=\"${basedir}/src/main/webapp/pictures\" /> "
			+ "<symlink link=\"${basedir}/src/main/webapp/pictures\" resource=\"${env.OPENSHIFT_DATA_DIR}pictures\"/> "
			+ "</target>" + "</project>";

	public static Map<String, String> getMapFromCategoryList(
			List<QuestionCategory> list) {

		Map<String, String> map = new HashMap<String, String>();

		for (QuestionCategory category : list) {
			map.put(String.valueOf(category.getCategoryId()),
					category.getName());
		}

		return map;
	}

	public static byte[] getImageByteArray(CommonsMultipartFile file)
			throws IOException {
		InputStream is = file.getInputStream();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		BufferedImage img = ImageIO.read(is);
		int h = img.getHeight();
		int w = img.getWidth();
		if (h > DEFAULT_HEIGHT) {
			h = DEFAULT_HEIGHT;
		}
		if (w > DEFAULT_WIDTH) {
			w = DEFAULT_WIDTH;
		}
		img = resize(img, w, h);

		ImageIO.write(img, "jpg", outputStream);
		outputStream.flush();
		byte[] imageInByte = outputStream.toByteArray();
		outputStream.close();
		is.close();
		return imageInByte;
	}

	public static BufferedImage resize(BufferedImage img, int newW, int newH) {
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage dimg = new BufferedImage(newW, newH, img.getType());
		Graphics2D g = dimg.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);
		g.dispose();
		return dimg;
	}
	// public static void runAnt() {
	// try {
	// String buildFileName = "build.xml";
	// File buildFile = new File(buildFileName);
	// FileOutputStream fo = new FileOutputStream(buildFile);
	// fo.write(buildFileStr.getBytes());
	// fo.flush();
	// fo.close();
	//
	// log.debug("{}", buildFile.getAbsoluteFile());
	// Project p = new Project();
	// p.setUserProperty("ant.file", buildFile.getAbsolutePath());
	// p.init();
	// ProjectHelper helper = ProjectHelper.getProjectHelper();
	// p.addReference("ant.projectHelper", helper);
	// helper.parse(p, buildFile);
	// p.executeTarget(p.getDefaultTarget());
	// buildFile.delete();
	// } catch (FileNotFoundException e) {
	// log.error("FileNotFoundException ",e);
	// e.printStackTrace();
	// } catch (BuildException e) {
	// log.error("BuildException ",e);
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// log.error("IOException ",e);
	// }
	// }
}
