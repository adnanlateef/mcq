<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
	
<title>Add Question</title>
<script type="text/javascript">
function saveAndMore() {
	$( "form" ).attr('action', '/add/question');
	$( "form" ).submit();
}
function saveAndShow() {
	$( "form" ).attr('action', '/show/question');
	$( "form" ).submit();
}
</script>
</head>
<body>
	<form:form method="post" action="/show/question" enctype="multipart/form-data" commandName="Question">
 		
	     <div>
			<form:select path="category.categoryId" maxlength="80" cssClass="textField">
				<form:options items="${categories}" />
			</form:select>
	     </div>
		<form:hidden path="questionId" />
	     <div>
			<form:input path="title" maxlength="80" cssClass="textField"/>
	     </div>
	     <div>
			<form:textarea path="text" rows="7" cols="70"/>
	     </div>
	     <div>
			<form:input path="file" type="file"/>
	     </div>
			
		 <div>
		 	<%-- <c:out value="${fn:length(Question.answers)}"></c:out> --%>
		 	<c:forEach items="${Question.answers}" var="ans" varStatus="status">
		 	<form:hidden path="answers[${status.index}].answerId"/>
		 	<div>
		 		<c:out value="Corrent Answer"></c:out>
			 	<div>
			 		<form:checkbox path="answers[${status.index}].correct" value="1"/> <br>
			 		<form:textarea path="answers[${status.index}].text" rows="7" cols="70"/>
			 	</div>
			 	<div>
			 		<form:input path="answers[${status.index}].file" type="file"/>
			 	</div>
		 	</div>
		 	</c:forEach>
		 </div>			

	     <div>
	     	
		    <input type="button" onclick="saveAndMore();" value="Save and Add More" style="width: 175px; font-size: medium;margin-top: 5px;" > 
		    <input type="button" onclick="saveAndShow();" value="Save and Show" style="width: 175px; font-size: medium;margin-top: 5px;" > 
		    <input type="button" value="Cancel" style="width: 175px; font-size: medium;margin-top: 5px;" 
		     onclick="javascript:history.go(-1);"> 
	     </div>
		
	</form:form>
</body>
</html>