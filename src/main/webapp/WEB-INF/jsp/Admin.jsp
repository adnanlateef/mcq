<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
<head>
<title>Admin</title>
</head>

<body>
	<form:form method="post" action="/admin/addCategories" enctype="multipart/form-data" commandName="Question">
		<input type="submit" value="Generate Categories">
	</form:form>

</body>
</html>
