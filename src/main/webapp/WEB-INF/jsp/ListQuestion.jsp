<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML>

<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List Questions</title>
 <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<script type="text/javascript">
</script>
</head>
<body>

	<c:forEach items="${ListQuestions}" var="question" varStatus="status">
		<table border="1">
			<tr>
				<td width="2%"><b><c:out value="${status.index + 1}."></c:out></b></td>
				<td width="88%">${question.title}</td>
				<td>
					<input type="button" value="Edit" onclick="javascript:location.href='/edit/question/${question.questionId}'">&nbsp;
					<input type="button" value="Delete" onclick="javascript:location.href='/delete/question/${question.questionId}'">
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<img src="/thumbnail/question/${question.questionId}"/><br/>
					${question.text}
				</td>
			</tr>
			<c:forEach items="${question.answers}" var="answer" varStatus="aStatus">
				<c:if test="${answer.correct}">
			<tr>
				<td colspan="3">
					<img src="/thumbnail/answer/${answer.answerId}"/><br/>
					${answer.text}
				</td>
			</tr>
				</c:if>
			</c:forEach>
			
		</table>
		<hr/>
	</c:forEach>

</body>
</html>