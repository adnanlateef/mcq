<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html>
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
	
<title>Add Question</title>
<script type="text/javascript">

</script>
</head>
<body>

	     <div>
	     	Category:<c:out value="${Question.category.text}"/>
	     </div>
		
	     <div>
	     Title:<c:out value="${Question.title}"/>
	     </div>
	     <div>
	     Text:<c:out value="${Question.text}"/>
	     </div>
	     <div>
			<img src="/thumbnail/question/${Question.questionId}"/><br/>
	     </div>
		
		 <div>
			<c:forEach items="${Question.answers}" var="ans" varStatus="status">
		 	<div>
		 		Answer:
			 	<div>
				<c:out value="${ans.text}"></c:out>
			 	</div>
			 	<div>
				<c:out value="Correct:${ans.correct}"></c:out>
			 	</div>
			 	<div>
			 		<img src="/thumbnail/answer/${Question.questionId}"/><br/>
			 	</div>
		 	</div>
			</c:forEach>
		 </div>			

	     <div>
		    <input type="button" value="Back" style="width: 175px; font-size: medium;margin-top: 5px;" 
		     onclick="javascript:history.go(-1);"> 
	     </div>
</body>
</html>