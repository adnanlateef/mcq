<%@ page language="java" contentType="text/html; charset=utf-8"
 pageEncoding="utf-8"%>
   reason:<%=request.getAttribute("javax.servlet.error.message") %>
<html lang="en">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
	<TITLE>ACT - Error Page - <%=request.getAttribute("javax.servlet.error.status_code") %></TITLE>
    <style>
		html {
		        background: #000000;
		}
		body {
		        text-align: center;
		        vertical-align: middle;
		}
</style> </head>
  <body>
    <div><span style="font-family: Courier;color: white;font-size:250"><%=request.getAttribute("javax.servlet.error.status_code") %></span></div>
    <br/>
    <div><span style="font-family: Courier;color: white;font-size:20">
    <%=request.getAttribute("javax.servlet.error.message") %><br>
    <div>Reason:</div>
    
    <div id="reason"><%=request.getAttribute("javax.servlet.error.exception") %></div> 
    <br>
        <br>        <br>
      </span></div>
    <div><span style="font-family: Courier;color: white;font-size:15">Tell us
        how you ended up here<br>
        <a href="mailto:adnan@actcompressors.com/">adnan@actcompressors.com</a></span></div>
        <br>
    <div><span style="font-family: Courier;color: white;font-size:15">
        <a href="/">Home</a></span></div>
        
  </body>
</html>